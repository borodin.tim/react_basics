import React, { useState } from 'react';

import Card from '../UI/Card';
import ExpensesFilter from './ExpensesFilter'
import ExpensesList from './ExpensesList';
import ExpensesChart from './ExpensesChart';
import './Expenses.css';

const Expenses = (props) => {
    const [filterYear, setFilterYear] = useState('2022');

    const handleFilterSelection = (filterSelection) => {
        console.log('filter value selected!!!!: ', filterSelection);
        setFilterYear(filterSelection);
    };

    const filteredExpenses = props.expenses
        .filter(expense => expense.date.getFullYear() === parseInt(filterYear));

    return (
        <div>
            <Card className="expenses">
                <ExpensesFilter
                    selected={filterYear}
                    onFilterSelected={handleFilterSelection}
                />
                <ExpensesChart expenses={filteredExpenses} />
                <ExpensesList expenses={filteredExpenses} />
            </Card>
        </div>
    );
};

export default Expenses;