import React, { useState } from 'react';

import ExpenseForm from './ExpenseForm';
import './NewExpense.css';

const NewExpense = (props) => {
    const [showForm, setShowForm] = useState(false);

    const onSaveExpenseDataHandler = (enteredExpenseData) => {
        const expenseData = {
            ...enteredExpenseData,
            id: Math.random().toString()
        };
        handleShowForm();
        props.onAddExpense(expenseData);
    };

    const handleShowForm = () => {
        setShowForm(prevState => prevState = !prevState);
    };

    return (
        <div className="new-expense">
            {showForm ?
                <ExpenseForm
                    onSaveExpenseData={onSaveExpenseDataHandler}
                    onCancel={handleShowForm}
                />
                :
                <button onClick={handleShowForm}>Add New Expense</button>
            }
        </div>
    );
};

export default NewExpense;